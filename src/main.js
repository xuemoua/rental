import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./assets/css/tailwind.css";
import VueCardFormat from "vue-credit-card-validation";

createApp(App).use(store).use(router).use(VueCardFormat).mount("#app");
