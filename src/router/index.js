import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Booking from "../components/Booking.vue"
import CarDetail from "../components/CarDetail.vue"
import SearchResults from "../components/SearchResults.vue"
import ConfirmBooking from "../components/ConfirmBooking.vue"
import Rental from "../components/RentalCar.vue"
import YourDetail from "../components/YourDetail.vue"

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/booking",
    name: "Booking",
    component: Booking,
  },
  {
    path: "/cardetail",
    name: "CarDetail",
    component: CarDetail,
  },
  {
    path: "/searchresults",
    name: "SearchResults",
    component: SearchResults,
  },
  {
    path: "/confirmbooking",
    name: "ConfirmBooking",
    component: ConfirmBooking,
  },
  {
    path: "/rental",
    name: "Rental",
    component: Rental,
  },
  {
    path: "/yourdetail",
    name: "YourDetail",
    component: YourDetail,
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
